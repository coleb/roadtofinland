# Roadtofinland - Summer 2020 Internships - Engineering Pre-assignment

### :rocket: A live demo of the front part of this project is available [here](https://coleb.gitlab.io/roadtofinland/)


This project is a mandatory pre-assignment task for Wolt's engineering intern positions.
The original subject can be found on this [GitHub](https://github.com/woltapp/summer2020) page.

> ### Overview
> A list of restaurants is an important element in Wolt app. Depending on where our customers are located, they will be able to order food from few dozen or even from hundreds of different restaurants. Views in the app (Discovery, Search, Delivers, Nearby) sort restaurants based on the delivery time, popularity, rating and various other factors when trying to find the most relevant matches for each customer.
> Your task is to either display a list of restaurant using the given data set (Option 1) OR create an API that allows searching restaurants from the data set (Option 2).  

I took some liberty about the given subject :
- A dropdown list allow users to filters restaurant by type of food.
- A restaurant details modal give some extra informations when clicking on a restaurant.

![Dashboard picture](https://zupimages.net/up/20/06/85ky.png)

### :rocket: A live demo of the front part of this project is available [here](https://coleb.gitlab.io/roadtofinland/)

## Run the project locally

Download or clone the project.

Run `npm install` in order to install dependencies.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
Generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.
